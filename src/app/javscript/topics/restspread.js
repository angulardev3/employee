//exampl-2
//rest operator
// function addNumber(a,b,...other){
//     console.log(other);
//     return a+b;
// }
// let rest=addNumber(2,3,4,5,6,7);
// console.log(rest)

// example-2
//spread operator
// The spread operator is a new addition to the set of operators in JavaScript ES6. 
// It takes in an iterable (e.g an array) and expands it into individual elements
// var names=["Manoj","Shiva","Beer","Ram"];
// function getNames(name1,name2,name3,name4){
//     console.log(name1,name2,name3,name4)
// }
// getNames(...names)

//how spread and rest with object
// example-3
var students={
    name:'Manoj',
    age:32,
    tech:["angular","typescript"]
}
// const age=students.age
// console.log(age);
// const {age}=students //destructuring
// console.log(age);
// //rest operator
// const {...rest}=students;
// console.log(rest);

//sread operator we can over rite object from new value
const newVal={
    ...students,
    age:36,
    tech:['raj']
}
console.log(newVal);
