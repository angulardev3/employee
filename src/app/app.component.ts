import { Component } from '@angular/core';
import {map,of} from 'rxjs'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss','./app.component.css']
})
export class AppComponent {
  title = 'practice';
  array=[2,3,4]
  constructor(){
    const result=of(1,2,3,4);
    result.pipe(map(x=>x*10)).subscribe(x=>console.log(x))
    console.log("result",result);
  }
}
