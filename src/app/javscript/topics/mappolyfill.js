//map, filter & array
//map method creates a new from existing array;
// let nums=[1,2,3,4];
// const newValue=nums.map((num,i,array)=>{
//     return num*2+i;
// });
// console.log(newValue)//[2,,4,6,8]

//filter provied those array which fullfills the provided criteria
//based condition it exists
// let nums=[1,2,3,4]
// const newValue=nums.filter((num)=>{
//     return num>3;
// });
// console.log(newValue);

//reduce take all the elements in an array & reduce them into single value;
// const num=[1,2,3,4,5];
// const sum=num.reduce((acc,curr,i,arr)=>{
//     return curr+acc;
// });
// console.log(sum)

//polyfill for map
// Array.prototype.myMap=function(c){
//     let temp=[];
//     for(let i=0;i<this.length;i++){
//         console.log(c(this[i],i,this));

//         temp.push(c(this[i],i,this));
//     }
//     return temp;
// };
// const num=[1,2,3,4];
// const multiply=num.myMap((nums)=>{
//     return nums*3;
// })
// console.log(multiply);

// qst 1: map vs foreach
// const arr=[2,3,5,6];
// const mapResult=arr.map((num)=>{
//     return num+2;
// })

// const forEachResult=arr.forEach((num,i)=>{
//     // return num+2;
//     arr[i]=num+3
// })
// console.log(mapResult);
// console.log(forEachResult,arr);

