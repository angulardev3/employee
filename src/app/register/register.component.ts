import { Component } from '@angular/core';
import {ApiService} from '../sharedservice/api.service'
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
name:string="";
address:string="";
phone:string="";
constructor(private api:ApiService,
  ){}

register(){
let bodyData={
  "name":this.name,
  "address":this.address,
  "phone":this.phone
}
this.api.registerData(bodyData).subscribe(res=>{
  console.log(res);
})
}
}
