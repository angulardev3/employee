import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http'
import {Observable, map} from 'rxjs'
import { Customer } from 'src/app/Modal/Customer'

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http:HttpClient) { }
  postEmployee(data:any){
    return this.http.post<any>("http://localhost:3000/posts",data)
    .pipe(map((res:any)=>{
      return res;
    }
    ))
  }

  getEmployee(){
    return this.http.get<any>("http://localhost:3000/posts")
    .pipe(map((res:any)=>{
      return res;
    }
    ))
  }

  updateEmployee(data:any,id:number){
    return this.http.put<any>("http://localhost:3000/posts/"+id,data)
    .pipe(map((res:any)=>{
      return res;
    }
    ))
  }

  deleteEmployee(id:number){
    return this.http.delete<any>("http://localhost:3000/posts/"+id)
    .pipe(map((res:any)=>{
      return res;
    }))
  }
  registerData(bodyData:any){
    console.log("bodyData",bodyData)
    return this.http.post<any>("http://localhost:3000/user/create",bodyData,{responseType:"json"})
    .pipe(map((res:any)=>{
      console.log("response",res)
      return res;
    }))
  }

  getCustomer():Observable<any>{
    return this.http.get<any>("http://localhost:3000/customers")
  
  }
}

