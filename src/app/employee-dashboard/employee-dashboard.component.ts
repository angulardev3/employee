import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from '../sharedservice/api.service';
import { EmployeeModel } from './empolye-dashboard.model';

@Component({
  selector: 'app-employee-dashboard',
  templateUrl: './employee-dashboard.component.html',
  styleUrls: ['./employee-dashboard.component.css']
})
export class EmployeeDashboardComponent {
  formValue !:FormGroup;
  constructor(private formbuilder:FormBuilder,private api:ApiService){}
  employeeModelObj !:EmployeeModel;
  employeeData:any;
  getId:any;
  showAdd !:Boolean;
  showUpdate !:Boolean;
  ngOnInit():void{
    this.formValue=this.formbuilder.group({
      firstName:[""],
      lastName:[""],
      email:[""],
      mobile:[""],
      salary:[""]
    })
    this.getEmployeeDetails();
  }
  postEmployeeDetails(){
    const employeeModelObj={
      firstName:this.formValue.value.firstName,
      lastName:this.formValue.value.lastName,
      email:this.formValue.value.email,
      mobile:this.formValue.value.mobile,
      salary:this.formValue.value.salary,

    }
  
  this.api.postEmployee(employeeModelObj).subscribe(res=>{
    console.log(res);
    alert("employee details added");
    let ref=document.getElementById("cancel");
    ref?.click();
    this.formValue.reset();
    this.getEmployeeDetails();
  },
  error=>{
    alert("something went wrong");
  })
  }
  getEmployeeDetails(){
    this.api.getEmployee().subscribe(res=>{
      console.log(res);
      this.employeeData=res;
    })
  }
  clickAddEmployee(){
    this.formValue.reset();
    this.showAdd=true
    this.showUpdate=false;
  }
  deleteEmployee(row:any){
    this.api.deleteEmployee(row.id)
    .subscribe(res=>{
      alert("employee dleted");
      this.getEmployeeDetails();
    })
  }
  // updateEmployee(row:any){
  //   this.api.updateEmployee(this.employeeModelObj,row.id).subscribe(res=>{
  //     alert("updated employee");
  //     this.getEmployeeDetails();
  //   })
  // }
  onEdit(row:any){
    this.showAdd=false
    this.showUpdate=true;
    this.getId=row.id;
    this.formValue.controls["firstName"].setValue(row.firstName);
    this.formValue.controls["lastName"].setValue(row.lastName);
    this.formValue.controls["email"].setValue(row.email);
    this.formValue.controls["mobile"].setValue(row.mobile);
    this.formValue.controls["salary"].setValue(row.salary);
  }

  updateEmployeeDetails(){
  
    const employeeModelObj={
      firstName:this.formValue.value.firstName,
      lastName:this.formValue.value.lastName,
      email:this.formValue.value.email,
      mobile:this.formValue.value.mobile,
      salary:this.formValue.value.salary,
    }
    this.api.updateEmployee(employeeModelObj,this.getId).subscribe(res=>{
      alert("updated successfully");
      let ref=document.getElementById("cancel");
      ref?.click();
      this.formValue.reset();
      this.getEmployeeDetails();
    })
  }
}
