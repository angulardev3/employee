console.log("9">"11");

// *******Note we can print by var also
// function x(){
//     for(let i=0;i<=5;i++){
//             setTimeout(function (){
//                 console.log(i);
//             },1000);
        
//     }
// }
// x();
// function x(){
//     var i=1;
//     setTimeout(()=>{
//         console.log(i);

//     },2000);
//     console.log("Learn");
// }
// x();

// function x(){
//     for(var i=0; i<=10; i++){
//         setTimeout(()=>{
//             console.log(i)
//         },1000);
//     }
// }
// x();




// let value="abboa";

// function isPalindrome(str){
//     const isValue= str.split("").reverse().join();
//     return isValue==str;
// }
// console.log(isPalindrome(value));

// function outer(){
//     let x=5;
//    function inner(){
//     console.log(x)
//    }
//     inner();
// }
// outer();




// function power(x){
//     return function(y){
//         if(y===0) return 1;
//         return x * power(x)(y-1)
//     }
// }
// const result=power(2)(4);
// console.log(new Date());//output 16
// let rows = 6;
// let char = "";

// for (let i = 1; i <= rows; i++) {
//   // printing spaces
//   for (let j = 0; j < rows - i; j++) {
//     char += " ";
//   }
//   // printing star
//   for (let k = 0; k < i; k++) {
//     char += "*";
//   }
//   char += "\n";
// }
// console.log(char);

// for(let i=1;i<5;i++){
//     for(let j=5;j>i;j--){
//         document.write("&nbsp")
//     }
//     for(let k=1;k<=(2*i)-1;k++){
//         document.write("*")
//     }
//     document.write("<br>")
// }

// function createPyramid(rows)
// {
//     for (let i = 0; i < rows; i++) {
//         var output = '';
//         for (let j =0; j < rows - i; j++) output += ' ';
//         for (let k = 0; k <= i; k++) output += '* ';
//         console.log(output);  
//     } 

// }
// createPyramid(5);
// let rows = 5;



// pattern variable carries the final pattern in string format
// let pattern = "";

// // outer loop runs for `rows` no. of times
// for (let n = 1; n <= rows; n++) {
//    // Inner Loop - I -> prints spaces
//    for (let space = 1; space <= rows - n; space++) {
//       pattern += " ";
//    }
//    // Inner Loop - II -> prints stars
//    for (let num = 1; num <= n; num++) {
//       pattern += "*";
//    }
//    pattern += "\n";
// }
// console.log(pattern);

// const arr = [1,2,2,3,1,5,5,7,8,9,1,5,7,8]
// const unique = arr.filter((v,i,a)=> a.indexOf(v) === i) // unique array
// const sorted = unique.sort(); // sorted array
// const reversed = sorted.reverse() // reverse array
// console.log(reversed)
// const secondLast = reversed[1]
// console.log(secondLast) // second largest

// var a = 'The quick brown fox jumps over a little lazy dog';
// var newA = a.split('');
// var b = ['a','e','i','o','u']
// var final = []
// var final1 = []
// for(let i=0; i<b.length; i++){
// for(let j=0; j<newA.length; j++){
// if(b[i] === newA[j]){
// final.push(1)
// final1.push(b[i])
// }
// }
// }
// console.log(final.length,final1.length) // vowels count

// var a = [1,2,3,4,5,3,5,6,1];
// var b = [];
// for(var i=0;i<a.length;i++){
// if(b.indexOf(a[i]) !== -1) {
// a[i] = 0;
// } else {
// b.push(a[i]);
// }
// }
// console.log(a) // [ 1, 2, 3, 4, 5, 0, 0, 6, 0 ]

// let obj1 = { a:10, b:20 }
// let obj2 = { a:10, b:20 }
// // console.log( obj1 === obj2, obj1 == obj2 )
// // Output: false false // can’t compare using operators
// console.log(JSON.stringify(obj1) === JSON.stringify(obj2))
// console.log(JSON.stringify(obj1) == JSON.stringify(obj2))

//prototype
// function employee(name, jobtitle, born) {
//     this.name = name;
//     this.jobtitle = jobtitle;
//     this.born = born;
    
//   }
//   employee.prototype.salary = 2000;
//   employee.prototype.notice = "immediate joiner";
//   const fred = new employee("Fred Flintstone", "Caveman", 1970);
  
//   console.log("prototype method",fred.salary,fred.notice);

// let num = 15;
// let text = num.toString();
// console.log(text)
