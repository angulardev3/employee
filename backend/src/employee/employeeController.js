var employeeModel=require('./employeeModel');

var createUserControllerFn=async(req,res)=>{

    try{
        const body=req.body;
        const employeeModelData= new employeeModel();
        employeeModelData.name=body.name;
        employeeModelData.address=body.address;
        employeeModelData.phone=body.phone;
        await employeeModelData.save();
        res.status(200).send({
            "status":true,"message":"Employee created successfully"
        });
    }
    catch{
        
        // res.status(400).send(err);
    }
}

module.exports=
{createUserControllerFn}