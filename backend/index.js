const express = require('express');
var cors=require('cors');
var app = express()
app.use(cors());
app.use(express.json());            

var mongoose=require('mongoose');

var routes=require('./routes/routes');
mongoose.connect('mongodb://127.0.0.1:27017/employee')
  .then(() => console.log('Connected!'));
app.get('/', function (req, res) {
  res.send('Hello World')
})

app.listen(3000);
app.use(routes)
app.use(express.json());