// write a function which reverse string

// Method-1
let str="string Reverse here";
function myString(str){
    return str.split("").reverse().join("");
}
console.log(myString(str))

// Method-2
// function myFunction(str){
//     let reversed="";
//     for(let i=str.length-1; i>=0;i--){
//         reversed += str[i];
//    } 
//    return reversed;
// }
// console.log(myFunction(str))


