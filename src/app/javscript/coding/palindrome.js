// write a function given string is palindrome or not
// A string is a palindrome if it is read the same from forward or backward ex: RADAR
console.log(palindromeFunction("racecar"));

function palindromeFunction(sentence){
    const reverseStr=sentence.split("").reverse().join("");
    return sentence===reverseStr;
}