let num=[20,30,40,50];
let test="Manoj"
// for loop
// for(let i=0;i<num.length;i++){
//     console.log("numbers loops",i+i);
// }

//foreach
// num.forEach(element => {
//     console.log("foreach element",element+element)
// });

// Array From
// let nameMine="Manoj";
// let arr=Array.from(nameMine);
// console.log("array from here",arr)

// // Array for.. of
// for(let i of num){
//     console.log("for of array", i)
// }
//for.. in
// for(let i in num){
//     console.log("for in arra",i)
// }

// for(let i of test){
//     console.log("for of string",i)
// }
// for(let i in test){
//     console.log("for in of string",i)
// }
// const testset=new Set([2,4,6,3,4,5])
// for(let i of testset){
//     console.log("test of set",i)
// }

// //map()
// let a=num.map((value,index,array)=>{
//     console.log(index);
//     // console.log(array)
//     return value+1;
// })
// console.log("array map value",a)

//filter
let a2=num.filter((a)=>{
    return a<10;
})
console.log("array filter",a2)