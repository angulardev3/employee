import { Component } from '@angular/core';
import { FormGroup,FormControl,Validators } from '@angular/forms';
@Component({
  selector: 'app-login-material',
  templateUrl: './login-material.component.html',
  styleUrls: ['./login-material.component.scss']
})
export class LoginMaterialComponent {
loginForm:any=FormGroup;
hide = true;
constructor(){};

ngOnInit(){
  this.loginForm=new FormGroup({
    email: new FormControl('',[Validators.required,Validators.email]),
    password: new FormControl('',[Validators.required,Validators.minLength(6)])
  })
}
onLogin(){

}
}
