import { Component, OnInit,ViewChild } from '@angular/core';
import { ApiService } from '../sharedservice/api.service';
import { Customer } from '../Modal/Customer'
import { MatTableDataSource } from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit  {

  customerList!:Customer[];
  datasource:any;
  displayedColumns:string[]=["code","name","email","phone","status","action"]
  @ViewChild(MatPaginator) paginator !:MatPaginator;
  @ViewChild(MatSort) sort !:MatSort;
  constructor(private api:ApiService){
  }
  ngOnInit(){
    this.api.getCustomer().subscribe(res=>{
      this.customerList=res;
      this.datasource=new MatTableDataSource<Customer>(this.customerList);
      this.datasource.paginator=this.paginator;
      this.datasource.sort=this.sort;
    })
  }
  filterChange(data:Event){
    console.log(data);
    const value=(data.target as HTMLInputElement).value;
   
    this.datasource.filter=value;
  }
}
