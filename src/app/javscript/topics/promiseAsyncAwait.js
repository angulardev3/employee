//In javascript promise is way to handle asynchronous operations.It is used find aynchronous 
// operation is successfully copmleted or not.
// Promise may have three states: Pending,Fulfilled,Rejected.
// let promise=new Promise(function(resolve,reject){
//     const isBoard=true;
//     if(isBoard){
//         resolve("you are right");
//     }else{
//         reject("you are wrong");
//     }
// }) 
// promise.then(()=>{
//     console.log("rseolve")})
// .catch(()=>{
//     console.log("rejected")})
// .finally(()=>{
//     console.log("I will always be executed")
// })

// example:1
// function getChees(){
//     return new Promise((resolve,reject)=>{
//         setTimeout(()=>{
//             const arr=[1,2,3,4];
//             resolve(arr);
//         },2000);
//     })
// }
// getChees().then((arr)=>{
//     console.log("I here arr",arr)
// })
// console.log(getChees());
async function getChees(){
    const arra=[1,2,3]
    console.log(await arra)
    setTimeout(()=>{

    },2000)
}
getChees();